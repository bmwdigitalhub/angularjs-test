'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const postCssConfigPath = path.resolve(__dirname, './postcss.config.js');

module.exports = {
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    // No info in console
                    silent: true
                },
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.woff$/,
                loader: 'url-loader'
            },
            {
                test: /\.(woff2|eot|ttf)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[hash].[ext]'
                }
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[hash].[ext]'
                }
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'url-loader'
                    },
                    {
                        loader: 'svg-fill-loader'
                    }
                ]
            },
            {
                test: /angular-locale_.+.js/,
                loader: 'file-loader',
                options: {
                    name: 'i18n/[desc].[ext]'
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                config: postCssConfigPath
                            }
                        },
                    ]
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                config: postCssConfigPath
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new ExtractTextPlugin('css/styles.[hash].css'),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true,
            chunksSortMode: function (chunk1, chunk2) {
                let orders = ['vendor', 'app'];
                let order1 = orders.indexOf(chunk1.names[0]);
                let order2 = orders.indexOf(chunk2.names[0]);
                return order1 - order2;
            }
        }),
        new webpack.LoaderOptionsPlugin({
            debug: true
        })
    ],
    stats: {
        assets: false,
        version: false,
        hash: false,
        timings: false,
        children: false
    },
    entry: {
        vendor: './vendor.ts',
        app: './app.ts'
    },
    output: {
        path: path.resolve('dist'),
        filename: 'js/[name].[hash].js',
        publicPath: '/'
    },
    resolve: {
        extensions: [
            '.webpack.js',
            '.web.js',
            '.js',
            '.json',
            '.ts'
        ]
    },
    devServer: {
        overlay: true,
        contentBase: 'dist',
        inline: true,
        stats: {
            colors: true,
            hash: false,
            version: false,
            timings: false,
            assets: false,
            chunks: false,
            modules: false,
            reasons: false,
            children: false,
            source: false,
            errors: false,
            errorDetails: false,
            warnings: false,
            publicPath: false
        }
    },
    bail: true
};