module.exports = {
    plugins: [
        require('autoprefixer')({
            browsers: [
                '> 1% in ZA',
                'Last 2 versions',
                'ios 8',
                'safari 8'
            ]
        })
    ]
};