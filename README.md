# BMW Digital/Web Hub AngularJS/TypeScript test

## Introduction

This test aims to obtain an understanding of your skill level using AngularJS with TypeScript as this will be required should you be appointed at BMW.

You are required to finish an AngularJS application using the following technology:

* AngularJS
* TypeScript
* Bootstrap
* SASS
* Webpack

The application allows a dealer to find a vehicle which can be marketed to a customer. This application provides the 
initial interface for this function.

Dealers can market different types of stock:

* NSC Stock (Stock allocated to the head office in that country)
* Dealer Stock (Stock allocated to the dealer)
* Unpreferred Stock (Stock allocated to other dealers, which this dealer can sell)

Dealers can market stock in various stages of production:

* In Production (Stock currently on the production line at a plant)
* In Transit (Stock currently being transported from the plant to the dealer)
* At Dealer (Stock which already arrived at the dealer)

## Running the application

The application is already configured to use Webpack.

To start a Webpack dev server:
```bash
yarn start
```

The Webpack dev server listens on [http://localhost:8080/]().

## Submission

Once you have completed the test:

* Replace this README.md with information tell us (Please keep it light):
    * How you approached the project
    * Known issues
    * Test instructions
    * Any other information you want to share with us
* Create a ZIP file named BUILD.ZIP containing the build from the dist directory
    * Make sure only the latest build is included
* Commit your changes (with the ZIP containing the build)
* Push the result to YOUR public GitHub or BitBucket repository
* Provide us with the link to YOUR repository

**Do NOT:**
* fork our repository
* push your changes to our repository
* create a pull requests on our repository
* comment on our repository

This gives other candidates an unfair advantage as they can use your code 

## Basic structure of project

It is assumed that you are familiar with AngularJS using TypeScript and webpack.

The application is loaded from app.ts in the root directory.

All styles should be loaded from styles/main.scss.

## Tasks

You are required to build an application which performs the following actions:

* Provide a screen where the user can select a vehicle from a drop down list
    * The list should be populated using an API call to the backend (simulated by a JSON file using the link below) 
    * The user should be able to filter the vehicles in the drop down list on the following criteria:
        * Brand
        * Stock type
            * NSC Stock
            * Dealer Stock
            * Unpreferred Stock
        * Production status
            * In Production
            * In Transit
            * At Dealer
* On selection of a vehicle, the vehicle following vehicle details should be displayed along with an option to view more information
    * Vehicle description
    * Exterior image
    * Interior image
* When the user selects the link to view more information, a new page should be displayed which displays the full detail of the vehicle as well as an option to return to the search screen

## Bonus Tasks

* Change the drop down list to an auto complete list (i.e. the user should be able to type in details about the vehicle and see a reduced list to select from)
* Add a static header and footer
* Considering BMW/MINI CI in screen design
* Adding unit tests (for this Chai, Mocha and Protractor may be used)

## Acceptance criteria

* Only libraries already included in the project may be used
* Code should adhere to the coding style as defined in the provided tslint configuration
* The application should build succesfully
* Successfully running and using the application
* Bootstrap should be used to achieve the required screen design
* SASS should be used for stylesheets
* Stylesheets should be logically structured to ease maintainability (i.e. we don't want a single main.scss)
* Angular Router should be used to transition between pages
* Promises should be used for data retrieval
* The wire frames available in the _wireframes_ directory should be used as a guideline for screen design
* The list of vehicles should be populated on page load
* The list should display the description of the vehicle and last 7 characters of the VIN
* The filtering must be done in real time (i.e. filter the vehicles retrieved at page load to display the vehicles matching the criteria)
* The VIN should never be displayed 

## Links

Description | Link 
---|---
List of vehicles | https://bitbucket.org/bmwdigitalhub/angularjs-test-data/raw/ae8e057c5d0cd30979927e9f296370d1da9dba1f/vehicles.json
Vehicle detail | https://bitbucket.org/bmwdigitalhub/angularjs-test-data/raw/ae8e057c5d0cd30979927e9f296370d1da9dba1f/{VIN}.json

**Note** The vehicle detail link contains a placeholder {VIN} which need to be replaced with the value from the List of Vehicles retrieved.
For example:
For a VIN of A0000000004312399, the link would be https://bitbucket.org/bmwdigitalhub/angularjs-test-data/raw/ae8e057c5d0cd30979927e9f296370d1da9dba1f/A0000000004312399.json

## Linting the application

To lint the application:

```bash
yarn run lint
```

## Building the application

To build the application:

```bash
yarn run build
```
