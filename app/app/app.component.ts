import {Component} from "../shared/decorators";
import {IStateService} from "angular-ui-router";
import CarService from "../car/car.service";
import {ICar} from "../car/car.dto";
import IWindowService = angular.IWindowService;
import IRootScopeService = angular.IRootScopeService;

@Component({
    template: require("./app.component.html"),
})
export default class AppComponent {
    public static $inject = [
        "carService",
        "$window",
        "$rootScope",
        "$state",
    ];

    private carList: ICar[];

    constructor(private carService: CarService,
                private $window: IWindowService,
                private $rootScope: IRootScopeService,
                private $state: IStateService) {
    }

    public $onInit() {
        this.carService.getList();
    }
}
