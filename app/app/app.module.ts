import AppComponent from "./app.component";

export default angular
    .module("bmw.test.app.main", [])
    .component("app", AppComponent);
