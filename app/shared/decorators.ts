export const Component = (options: ng.IComponentOptions = {}) => controller => angular.extend(options, {controller});
