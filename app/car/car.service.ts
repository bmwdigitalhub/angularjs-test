import Constants from "../shared/constants";
import {ICar} from "./car.dto";
import IHttpService = angular.IHttpService;
import IPromise = angular.IPromise;

export default class CarService {
    public static $inject = [
        "$http",
    ];

    constructor(private $http: IHttpService) {
    }

    public getList(): IPromise<ICar[]> {
        return this.$http.get(Constants.API_VEHICLE_LIST)
            .then(data => {
                return [];
            })
            .catch(() => {
                return [];
            });
    }
}
