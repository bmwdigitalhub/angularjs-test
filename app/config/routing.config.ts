(() => {
    function routeConfig($urlRouterProvider: angular.ui.IUrlRouterProvider,
                         $stateProvider: angular.ui.IStateProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider.state("app", {
            url: "/",
            views: {
                "main@": {
                    template: "<app></app>",
                },
            },
        });
    }

    routeConfig.$inject = ["$urlRouterProvider", "$stateProvider"];

    angular
        .module("bmw.test.app")
        .config(routeConfig);

})();
