import AppModule from "../app/app.module";

(() => {
    angular
        .module("bmw.test.app", [
            "ui.router",
            AppModule.name,
        ]);

    angular.element(document).ready(() => {
        angular.bootstrap(document, ["bmw.test.app"], {strictDi: true});
    });
})();
